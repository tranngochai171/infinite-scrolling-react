import { useEffect, useState } from "react";
import axios from "axios";
function useBookSearch(query, pageNumber) {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [books, setBooks] = useState([]);
  const [hasMore, setHasMore] = useState(false);
  useEffect(() => {
    setBooks([]);
  }, [query]);
  useEffect(() => {
    let cancel;
    setLoading(true);
    setError(false);
    const fetchData = async () => {
      try {
        const res = await axios({
          method: "GET",
          url: "http://openlibrary.org/search.json",
          params: { q: query, page: pageNumber },
          cancelToken: new axios.CancelToken((c) => (cancel = c)),
        });
        setBooks((prevBook) => [
          ...new Set([...prevBook, ...res.data.docs.map((b) => b.title)]),
        ]);
        setLoading(false);
        setHasMore(res.data.docs.length > 0);
      } catch (error) {
        if (axios.isCancel(error)) return;
        setError(true);
      }
    };
    fetchData();
    return () => {
      cancel();
    };
  }, [query, pageNumber]);
  return { books, loading, error, hasMore };
}

export default useBookSearch;
